import { Component, OnInit, isDevMode } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {
  CLIENT_ID,
  LOGIN_URL,
  ISSUER_URL,
  SCOPE,
  PROMPT,
} from './constants/storage';
import { AppService } from './app.service';
import {
  OAuthService,
  AuthConfig,
  JwksValidationHandler,
  OAuthEvent,
} from 'angular-oauth2-oidc';
import { INDEX_HTML, SILENT_REFRESH } from './constants/routes';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
  loggedIn: boolean;
  hideAuthButtons: boolean = false;

  constructor(
    private readonly platform: Platform,
    private readonly splashScreen: SplashScreen,
    private readonly statusBar: StatusBar,
    private readonly appService: AppService,
    private readonly oauthService: OAuthService,
  ) {
    this.setupOIDC();
    this.initializeApp();
  }

  ngOnInit() {
    this.oauthService.events.subscribe(({ type }: OAuthEvent) => {
      // Silent Refresh
      switch (type) {
        case 'token_received':
          this.setUserSession();
          break;
        case 'logout':
          this.setUserSession();
          break;
      }
    });

    this.setUserSession();
  }

  setUserSession() {
    this.loggedIn = this.oauthService.hasValidAccessToken();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }

  login() {
    this.oauthService.initLoginFlow();
  }

  logout() {
    this.oauthService.logOut();
    this.loggedIn = false;
  }

  setupOIDC(): void {
    this.appService.getMessage().subscribe(response => {
      if (response.message) return; // { message: PLEASE_RUN_SETUP }
      this.appService.setInfoLocalStorage(response);
      this.appService.getService().subscribe(service => {
        const authConfig: AuthConfig = {
          clientId: localStorage.getItem(CLIENT_ID),
          redirectUri: `${service.serviceURL}${INDEX_HTML}`,
          silentRefreshRedirectUri: `${service.serviceURL}${SILENT_REFRESH}`,
          loginUrl: localStorage.getItem(LOGIN_URL),
          scope: SCOPE,
          issuer: localStorage.getItem(ISSUER_URL),
          disableAtHashCheck: true,
          customQueryParams: { prompt: PROMPT },
        };
        if (isDevMode()) authConfig.requireHttps = false;
        this.oauthService.configure(authConfig);
        this.oauthService.tokenValidationHandler = new JwksValidationHandler();
        this.oauthService.setupAutomaticSilentRefresh();
        this.oauthService.loadDiscoveryDocumentAndTryLogin();
      });
    });
  }
}
