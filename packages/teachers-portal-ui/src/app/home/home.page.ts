import { Component, OnInit } from '@angular/core';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';
import { HttpClient } from '@angular/common/http';
import { ISSUER_URL } from '../constants/storage';
import { IDTokenClaims } from '../common/interfaces/id-token-claims.interfaces';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean;
  picture: string;
  email: string;
  name: string;
  accessToken: string;

  constructor(
    private readonly oauth2: OAuthService,
    private readonly http: HttpClient,
  ) {}

  ngOnInit() {
    this.oauth2.events.subscribe(({ type }: OAuthEvent) => {
      // Silent Refresh
      switch (type) {
        case 'token_received':
          this.setUserSession();
          break;
        case 'logout':
          this.setUserSession();
          break;
      }
    });

    this.setUserSession();
  }

  setUserSession() {
    this.loggedIn = this.oauth2.hasValidAccessToken();
    this.accessToken = this.oauth2.getAccessToken();
    if (this.loggedIn) {
      const url = localStorage.getItem(ISSUER_URL) + '/oauth2/profile';
      this.http
        .get<IDTokenClaims>(url, {
          headers: { authorization: this.oauth2.authorizationHeader() },
        })
        .subscribe({
          next: response => {
            this.picture = response.picture;
            this.name = response.name;
            this.email = response.email;
          },
          error: error => {},
        });
    }
  }
}
